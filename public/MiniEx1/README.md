# AP 2020
![Screenshot](MiniEx1.PNG)

[Link](http://SofieBP.gitlab.io/ap-2020/MiniEx1)

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
It was exciting and quite difficult at the same time. It was a great help having access to the p5.js references. At first I was making some random circles and played with adding a thicker outlife, which made it look like a vinyl. So I thought about going with it and made some more. Then I tried to make them spin around. That was rather difficult since the code I was working with added sharp edges to the outline of the vinyls. Then I accidently made the inner circle move, almost in a hypnotizing way and went with that as well as changing colors. It was a fun process when creating something that actually works, but at the same time very frustrating when it doesn´t work the way you picture it working.

**How is the coding process different from, or similiar to, reading and writing text?**
The difference is that with coding you have to be very specific with the syntaxes you are writing. If there is a slight mistake it won´t work. So that made me very aware of what I was writing and reading while I was doing it. When you are writing or reading a regular text I find it that you don´t focus as much as what you are doing or how you are doing it. We are very used to reading and writeing so we know how to do it correctly by experience. And if I have made a mistake I disover it a little later, but it doesn´t interrupt or ruing the process.

**What does code and programming mean to you, and how do the assigned readings help you yo reflect on programming?**
It´s still a completely new field that I don´t know a lot about. I know it will be frustrating several more times but I´m still excited to explore the world of programming and expanding my knowledge and skills. It´s great with the assigned readings since it gives a lot of new information about programming which I can use to learn more. I also quite like the assigned video tutorials with Daniel Shiffman, those are quite helpful. 