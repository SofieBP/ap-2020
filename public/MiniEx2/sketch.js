
// for red, green, and blue color values
const R = 150;
const xh = angle => R / 15.0 * 16 * Math.pow(Math.sin(angle), 3);
const yh = p => R / 15.0 * (-13 * Math.cos(p) + 5 * Math.cos(2 * p) + 2 * Math.cos(3 * p) + Math.cos(4 * p));
let r, g, b;
function setup() {
  createCanvas(windowWidth, windowHeight);
  // Pick colors randomly
  r = random(255);
  g = random(255);
  b = random(255);
}

function draw() {
  background(255);
  // Emoji #1
  // Draw a circle - the face of the emoji
  strokeWeight(2);
  stroke(r, g, b);
  fill(r, g, b);
  stroke(0)
  strokeWeight(2)
  ellipse(290,290,500,500)

 // Left eye
fill(0);
ellipse(210,250,50,100);

// Right eye
ellipse(370,250,50,100);
noStroke();

// Mouth
arc(290, 390, 290, 200, radians(0), radians(180));

// Teeth
fill(255);
arc(290,393,286,90, radians(0), radians(180));

// Emoji #2
fill(0);
 stroke(0);
 strokeWeight(3);
 beginShape();
 let n = 200;
 for (let i = 0; i < n; i++) {
   let x = width / 1.5 + xh(TAU * i / n);
   let y = height / 2.5 + yh(TAU * i / n);
   vertex(x, y);
 }
 endShape();

 textSize(26);
  fill(255);
 text('Love', 915, 200);
  textSize(24);
  fill(255);
  text('Life', 885,225);
  textSize(30);
  fill(255);
  text('Earth',935,230);
  textSize(31);
  fill(255);
  text('People', 880,260);
  textSize(28);
  fill(255);
  text('It', 900,300);
  textSize(30);
  fill(255);
  text('Identity', 935, 345);
  textSize(23);
  fill(255);
  text('Representation', 990,260);
  textSize(24);
  fill(255);
  text('Friendship',970, 370)
  textSize(28);
  fill(255);
  text('Family', 1060,210);
  textSize(22);
  fill(255);
  text('Kindness', 1080,235)
  textSize(15);
  fill(255);
  text('Trust', 1035, 235);
  textSize(22);
  fill(255);
  text('Hope', 998, 390);
  textSize(15);
  fill(255);
  text('Words', 990, 405);
  textSize(33);
  fill(255);
  text('Truth', 1042, 345);
  textSize(15);
  fill(255);
  text('You', 1010, 422);
  textSize(59);
  fill(255, 0, 0);
 text('Matter(s)', 925, 310);
}

// When the user clicks the mouse
function mousePressed() {
  // Check if mouse is inside the circle
  let d = dist(mouseX, mouseY, 360, 200);
  if (d < 500) {
    // Pick new random color values
    r = random(255);
    g = random(255);
    b = random(255);
  }
}
