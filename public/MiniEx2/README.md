# AP 2020
![Screenshot](MiniEx2.PNG)

[Link](http://SofieBP.gitlab.io/ap-2020/MiniEx2)

**Link to the location of the index.html and j.s file**
https://gitlab.com/SofieBP/ap-2020/-/tree/master/public/MiniEx2

**Link to used refrences**
**Emoji #1**

Ellipse - used for the face and the eyes of the emoji
https://p5js.org/reference/#/p5/ellipse

Arc - used for the mouth and the teeth of the emoji
https://p5js.org/reference/#/p5/arc

Color change of the face
https://p5js.org/examples/hello-p5-interactivity-1.html

**Emoji #2**

Heart shape
https://editor.p5js.org/makio135/sketches/AUy_cG-d4

Text - 1st reference on the page 
https://p5js.org/reference/#/p5/text

**1. Describe the program and what you have used an learnt**

My first emoji in the program is like the regular yellow smiley face, except that it changes colors when clicked on. To create my first emoji I have used the syntax for ellipse and arch to create the face, eyes, mouth and teeth for the smiley face. Besides that I have used the syntax for fill, stroke, strokeWeight and noStroke. To make the emoji change colors I have used function mousePressed where it´s set to change between a random pallete of colors. I found this specific syntax by searching online. For this function to work it was neccesary to use variables as well.

My second emoji in the program is a heart with the word "matter(s)" in the center of the heart, and several other words placed around the center words. Here I have used the syntax for beginShape and endShape to create the heart, which I stumbled across online, because someone else had used those syntaxes to create the heart shape. Besides that I have used the syntax for creating text, including: textSize, fill and text.

For MiniEx2 I found it quite difficult figuring out what I wanted to make and what the message behind my emojies should be. I knew pretty quickly that I wanted one that could change color, and by the time I came to the process of making the colorchange happen it was really difficult. I tried several syntaxes and it wouldn´t work, but eventually I figures it out with some help from the WebEditor on p5.js. For the second emoji I struggled with creating in general and tried som many things and had no luck, it made me very frustrated if I´m honest. But in the end I found a way to create a vision I had and had a lot of fun making it. It was a great theme for a MiniEx and I´m excited for creating more and play around with JavaScript more. 

**2. How would you put your emojies into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond?**

From the chapter "Modyfying the Universal" from the text we have read, it´s mentioned how the addition of the skin tone modifiers in 2015 rose awareness of issues around the world associated with identity, representation, politics. Since that more and more people have the wish of feeling representated when using emojies so that they feel more personal than just small icons you send to family and friends. When introducing the modifiers the regular yellow smiley face emoji began to function as a white base, with darker skin colours positioned as an add-on. s. 40. Which created issues around the world as well. 

For the creating of my first emoji I thought about the regular yellow smiley face. How come it´s yellow? What if someone would rather have it being red, blue, green or any other color? What if they don´t like the fact that it´s yellow and that they can´t identify with it because blue is their color per se? That´s what I want to do with the first emoji. Just like you can choose different skin tones to your thumbs up, or choose different colored hearts, it should be possible to choose between other colors than yellow for the regular face emoji. That´s why I made an emoji that could change color when one press the mouse on the emoji.

For my second emoji I thought about either creating a heart showcasing that everyone is loved and should feel represented or a emoji focusing on climate change. I ended up with the heart idea. It represents that all the words in within the heart matter; love, life, family, identity and so on. The point is that it´s an emoji for everyone who might not feel like they are worth something or feel different somehow. 



