
var input, button, greeting; // Creates the buttons, the search input and the greeting

function setup() {
  // Create canvas
  createCanvas(windowWidth, windowHeight);

  // Input is the search bar that appears on screen
   input = createInput();
    input.position(630, 200);

  // Create the submit button
   button = createButton('submit');
    button.position(input.x + input.width, 200); // input.width follows the widt of the search bar
     button.mousePressed(greet);

  // Text/greeting that appears at the top. h1 and h2 stand for headline and the number is the headline size
   greeting = createElement('h1', 'Hello stranger, pleasure to meet you!'); // Top phrase
    greeting.position(500, 8);
   greeting = createElement('h2', 'Begin by telling a little about yourself plaese.'); // Middle top phrase
     greeting.position(525, 60);
   greeting = createElement('h2', 'What do you like, dislike or simply find interesting?'); // Middle bottom phrase
     greeting.position(490, 90);
   greeting = createElement('h2', 'You can write as little or as much as you like.'); // Bottom phrese
     greeting.position(520, 120);

  // Text size of the input that comes up on the screen after submitting
    textSize(50);



  // The button down the page you can click
   button = createButton('Thanks for sharing information about you, now click here');
     button.position(560, 500);
      button.mousePressed(personalInformation);

  // The reset button
   button = createButton('reset if you need a clean slate');
     button.position(630, 600);
      button.mousePressed(reset);
}

// The text that appears once the personalInformation button has been pressed
function personalInformation () {
  fill(0);
let words = ['Data saved. You are being watched at all time'];
let word = random(words); // select random words. Only one sentence here, but the function mess up without the random selecter
  textSize(70) // Size of the text that appears
text(word, 80, 400); // draw the word
}

// The reset button, clears the entire sketch once it´s clicked on
function reset () {
clear();
}

// greeting and function for the words to place randomly. Won´t work without function greet
function greet() {
  const name = input.value();

  for (let i = 0; i < 100; i++) {
    push();
    fill(random(255), 100, 255); 
    translate(random(width), random(height));
    rotate(random(2 * PI));
    text(name, 0, 0);
    pop();
  }
}
