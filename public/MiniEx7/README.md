# AP 2020

# The Circle(s)

Made by Mira Bella Dyring Morsø & Sofie Berg Pedersen 

# Link to the program
[Link](http://SofieBP.gitlab.io/ap-2020/MiniEx7)

# Link to the respository
https://gitlab.com/SofieBP/ap-2020/-/tree/master/public/MiniEx7

# Screenshot

![Screenshot](MiniEx7.png)

# Questions and Answers

***What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors?***

Every time the program runs a random vertex shall appear 
The random vertices shall rotate in a circular motion 
There needs to be an x amount of ellipses moving across the canvas in a for-loop at all times 
The vertices and ellipses chances colors constantly in a random selection 

***What's the role of rules and processes in your work?***

The rules we have set up, is what creates the final result. When using rules in general it’s a great help to set up guidelines for the programmer especially if you are having troubles with getting started right of the batch. 

***Draw upon the assigned reading(s), how does this mini-exercise help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?***

Marius Watz talks about creating a non-verbal experience of form and space when making Generative art. We have also created what he calls “a system external to yourself,” as we created it we faced different problems that in the end became a part of our program in the end.
Our program is probably what Watz calls a Weak Generative Art, since we are creating the program and the “parametric systems” that the program is running. It is therefore semi-automated. We used a lot of random variables to do this, and the program therefore becomes a co-author of the final result.
In this context the following quote from Watz makes a lot of sense:
“Not created by him specifically but by the movements of the pixels he’s creating.”
The high amount of randomness in our program is added to make the work more interesting and unpredictable. As Montfort talk about, randomness makes things much more fun and interesting to the one experiencing it. And that was the goal.



# References:

**“Move in a circle” by kchung**

*https://editor.p5js.org/kchung/sketches/SJkdHhWUQ

We used this reference to get a general idea of, how we could make a figure move around in a circular shape.

**“Generative art-Jeremy” by jsarachen**

*https://editor.p5js.org/jsarachan/sketches/rJMThoqA-

We used this reference to get inspiration for the type of shapes we wanted in our program. 
