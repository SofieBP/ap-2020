
var angle = 0;	// angle variable
var scalar = 300;  // radius of circle
let bubble = []; // array for the bubbles
let i = 0;
let min_bubble = 20; // minimum amount of bubbles on screen

function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);	// change  angle mode from radians to degrees
  frameRate(100);
  background(255,255,255);

  for (let i = 0; i <= min_bubble; i++){
    bubble[i] = new Bubble();
  }
}

function draw() {
  var x = windowWidth/2 + scalar * cos(angle);
  var y = windowHeight/2 + scalar * sin(angle);

// randomly picked colors
  fill(random(255),random(255),random(255),random(255));
  noStroke();

// random vertex' created within the circle
// (x,y); makes the figures have a corner in the circle
  push();
  beginShape();
    vertex(random(windowWidth/2 - scalar * cos(angle), windowWidth/2 + scalar * cos(angle)),random(windowHeight/2 - scalar * sin(angle), windowHeight/2 + scalar * sin(angle)));
    vertex(x,y);
    vertex(random(windowWidth/2 - scalar * cos(angle), windowWidth/2 + scalar * cos(angle)),random(windowHeight/2 - scalar * sin(angle), windowHeight/2 + scalar * sin(angle)));
  endShape();
  pop();

// increasing angle for the next frame
  angle++;

// makes more bubbles appear when bubbles have passed x - called in draw so the program understands the function further down
showBubble();
checkBubbleNum();
if(bubble[i].y>height || bubble[i].x>width){
  bubble.splice(i,1);
  newBubble();
  }
}

// makes the bubbles appear
function showBubble(){
  for (let i = 0; i <bubble.length;i++) {
    bubble[i].show();
    bubble[i].move();
  }
}

// push new bubbles if the amount of bubbles on screen < min_bubble
function checkBubbleNum() {
  if (bubble.length < min_bubble) {
    bubble.push(new Bubble());
  }
}

// splice bubbles when they pass x and then creates new ones
function spliceBubble() {
  if (bubble[i].y>height || bubble[i].x>width){
    bubble.splice(i,1);
    newBubble();
  }
}

// pushes new bubbles when the amount of bubbles < min_bubble
function newBubble() {
  if (bubble.length < min_bubble) {
    bubble.push(new Bubble(random(-400,-500)));
  }
}

class Bubble {
  constructor(x,y) {
    this.x = random(-windowHeight);
    this.y = random(-windowWidth);
  }

  move () {
    this.x = this.x + 10;
    this.y = this.y + 10;
  }

show() {
  fill(random(255),random(255),random(255),random(255));
    noStroke();
    ellipse (this.x, this.y, 10, 10);
  }
}
