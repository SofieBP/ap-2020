var song1, song2, song3, song4, song5, song6, song7, song8, song9, song10, song11, song12, song13, song14, song15, song16, song17, song18, song19, song20, song21;
var button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13, button14, button15, button16, button17, button18, button19, button20, button21;

function setup() {
  createCanvas(windowWidth, windowHeight);

  // All of the buttons to play the music
  button1 = createButton('play 1')
  button1.position(70, 200);
   song1 = loadSound("eyeofthetiger.mp3", loaded);
  button1.mousePressed(song1Play);

  button2 = createButton('play 2')
  button2.position(270, 200);
   song2 = loadSound("beatit.mp3", loaded);
  button2.mousePressed(song2Play);

  button3 = createButton('play 3')
  button3.position(470, 200);
   song3 = loadSound("wakemeup.mp3", loaded);
  button3.mousePressed(song3Play);

  button4 = createButton('play 4')
  button4.position(670, 200);
   song4 = loadSound("nevergonnagiveyouup.mp3", loaded);
  button4.mousePressed(song4Play);

  button5 = createButton('play 5')
  button5.position(870, 200);
   song5 = loadSound("jailhouserock.mp3", loaded);
  button5.mousePressed(song5Play);

  button6 = createButton('play 6')
  button6.position(1070, 200);
   song6 = loadSound("footloose.mp3", loaded);
  button6.mousePressed(song6Play);

  button7 = createButton('play 7')
  button7.position(1270, 200);
   song7 = loadSound("september.mp3", loaded);
  button7.mousePressed(song7Play);

  button8 = createButton('play 8')
  button8.position(70, 450);
   song8 = loadSound("stayingalive.mp3", loaded);
  button8.mousePressed(song8Play);

  button9 = createButton('play 9')
  button9.position(270, 450);
   song9 = loadSound("dontstopbelieving.mp3", loaded);
  button9.mousePressed(song9Play);

  button10 = createButton('play 10')
  button10.position(470, 450);
   song10 = loadSound("africa.mp3", loaded);
  button10.mousePressed(song10Play);

  button11 = createButton('play 11')
  button11.position(670, 450);
   song11 = loadSound("allstar.mp3", loaded);
  button11.mousePressed(song11Play);

  button12 = createButton('play 12')
  button12.position(870, 450);
   song12 = loadSound("cottoneyejoe.mp3", loaded);
  button12.mousePressed(song12Play);

  button13 = createButton('play 13')
  button13.position(1070, 450);
   song13 = loadSound("bringmebacktolife.mp3", loaded);
  button13.mousePressed(song13Play);

  button14 = createButton('play 14')
  button14.position(1270, 450);
   song14 = loadSound("callmemaybe.mp3", loaded);
  button14.mousePressed(song14Play);

  button15 = createButton('play 15')
  button15.position(70, 700);
   song15 = loadSound("despacito.mp3", loaded);
  button15.mousePressed(song15Play);

  button16 = createButton('play 16')
  button16.position(270, 700);
   song16 = loadSound("webuiltthiscity.mp3", loaded);
  button16.mousePressed(song16Play);

  button17 = createButton('play 17')
  button17.position(470, 700);
   song17 = loadSound("itsgonnabeme.mp3", loaded);
  button17.mousePressed(song17Play);

  button18 = createButton('play 18')
  button18.position(670, 700);
   song18 = loadSound("partyintheusa.mp3", loaded);
  button18.mousePressed(song18Play);

  button19 = createButton('play 19')
  button19.position(870, 700);
   song19 = loadSound("gangnamstyle.mp3", loaded);
  button19.mousePressed(song19Play);

  button20 = createButton('play 20')
  button20.position(1070, 700);
   song20 = loadSound("sk8terboi.mp3", loaded);
  button20.mousePressed(song20Play);

  button21 = createButton('play 21')
  button21.position(1270, 700);
   song21 = loadSound("moveslikejagger.mp3", loaded);
  button21.mousePressed(song21Play);

    background(255, 100, 255);
}

function draw() {

 // First row of records
    fill(random(255), random(255), random(255));
  ellipse(100, 100, 55, 55); // record one - song 1
    fill(random(255), random(255), random(255));
  ellipse(300, 100, 55, 55); // record two - song 2
    fill(random(255), random(255), random(255));
  ellipse(500, 100, 55, 55); // record three - song 3
    fill(random(255), random(255), random(255));
  ellipse(700, 100, 55, 55); // record four - song 4
    fill(random(255), random(255), random(255));
  ellipse(900, 100, 55, 55); // record 5 - song 5
    fill(random(255), random(255), random(255));
  ellipse(1100, 100, 55, 55); // record 6 - song 6
    fill(random(255), random(255), random(255));
  ellipse(1300, 100, 55, 55); // record 7 - song 7

 // Second row of records
    fill(random(255), random(255), random(255));
  ellipse(100, 350, 55, 55); // record 8 - song 8
    fill(random(255), random(255), random(255));
  ellipse(300, 350, 55, 55); // record 9 - song 9
    fill(random(255), random(255), random(255));
  ellipse(500, 350, 55, 55); // record 10 - song 10
    fill(random(255), random(255), random(255));
  ellipse(700, 350, 55, 55); // record 11 - song 11
    fill(random(255), random(255), random(255));
  ellipse(900, 350, 55, 55); // record 12 - song 12
    fill(random(255), random(255), random(255));
  ellipse(1100, 350, 55, 55); // record 13 - song 13
    fill(random(255), random(255), random(255));
  ellipse(1300, 350, 55, 55); // record 14 - song 14

 // Third row of records
    fill(random(255), random(255), random(255));
  ellipse(100, 600, 55, 55); // record 15 - song 15
    fill(random(255), random(255), random(255));
  ellipse(300, 600, 55, 55); // record 16 - song 16
    fill(random(255), random(255), random(255));
  ellipse(500, 600, 55, 55); // record 17 - song 17
    fill(random(255), random(255), random(255));
  ellipse(700, 600, 55, 55); // record 18 - song 18
    fill(random(255), random(255), random(255));
  ellipse(900, 600, 55, 55); // record 19 - song 19
    fill(random(255), random(255), random(255));
  ellipse(1100, 600, 55, 55); // record 20 - song 20
    fill(random(255), random(255), random(255));
  ellipse(1300, 600, 55, 55); // record 21 - song 21

  // Stroke - thick black outline
  strokeWeight(random(100));
}

// All of the songs - to make them play when you press the button
function song1Play() {
  if(!song1.isPlaying()) {
  song1.play();
  button1.html("pause 1");
} else {
  song1.stop();
  button1.html("play 1");
 }
}

 function song2Play() {
  if(!song2.isPlaying()) {
    song2.play();
    button2.html("pause 2");
  } else {
    song2.stop();
    button2.html("play 2");
  }
}

function song3Play() {
  if(!song3.isPlaying()) {
    song3.play();
    button3.html("pause 3");
  } else {
    song3.stop();
    button3.html("play 3");
  }
}

function song4Play() {
  if(!song4.isPlaying()) {
    song4.play();
    button4.html("pause 4");
  } else {
    song4.stop();
    button4.html("play 4");
  }
}

function song5Play() {
  if(!song5.isPlaying()) {
    song5.play();
    button5.html("pause 5");
  } else {
    song5.stop();
    button5.html("play 5");
  }
}

function song6Play() {
  if(!song6.isPlaying()) {
    song6.play();
    button6.html("pause 6");
  } else {
    song6.stop();
    button6.html("play 6");
  }
}

function song7Play() {
  if(!song7.isPlaying()) {
    song7.play();
    button7.html("pause 7");
  } else {
    song7.stop();
    button7.html("play 7");
  }
}

function song8Play() {
  if(!song8.isPlaying()) {
    song8.play();
    button8.html("pause 8");
  } else {
    song8.stop();
    button8.html("play 8");
  }
}

function song9Play() {
  if(!song9.isPlaying()) {
    song9.play();
    button9.html("pause 9");
  } else {
    song9.stop();
    button9.html("play 9");
  }
}

function song10Play() {
  if(!song10.isPlaying()) {
    song10.play();
    button10.html("pause 10");
  } else {
    song10.stop();
    button10.html("play 10");
  }
}

function song11Play() {
  if(!song11.isPlaying()) {
    song11.play();
    button11.html("pause 11");
  } else {
    song11.stop();
    button11.html("play 11");
  }
}

function song11Play() {
  if(!song11.isPlaying()) {
    song11.play();
    button11.html("pause 11");
  } else {
    song11.stop();
    button11.html("play 11");
  }
}

function song12Play() {
  if(!song12.isPlaying()) {
    song12.play();
    button12.html("pause 12");
  } else {
    song12.stop();
    button12.html("play 12");
  }
}

function song13Play() {
  if(!song13.isPlaying()) {
    song13.play();
    button13.html("pause 13");
  } else {
    song13.stop();
    button13.html("play 13");
  }
}

function song14Play() {
  if(!song14.isPlaying()) {
    song14.play();
    button14.html("pause 14");
  } else {
    song14.stop();
    button14.html("play 14");
  }
}

function song15Play() {
  if(!song15.isPlaying()) {
    song15.play();
    button15.html("pause 15");
  } else {
    song15.stop();
    button15.html("play 15");
  }
}

function song16Play() {
  if(!song16.isPlaying()) {
    song16.play();
    button16.html("pause 16");
  } else {
    song16.stop();
    button16.html("play 16");
  }
}

function song17Play() {
  if(!song17.isPlaying()) {
    song17.play();
    button17.html("pause 17");
  } else {
    song17.stop();
    button17.html("play 17");
  }
}

function song18Play() {
  if(!song18.isPlaying()) {
    song18.play();
    button18.html("pause 18");
  } else {
    song18.stop();
    button18.html("play 18");
  }
}

function song19Play() {
  if(!song19.isPlaying()) {
    song19.play();
    button19.html("pause 19");
  } else {
    song19.stop();
    button19.html("play 19");
  }
}

function song20Play() {
  if(!song20.isPlaying()) {
    song20.play();
    button20.html("pause 20");
  } else {
    song20.stop();
    button20.html("play 20");
  }
}

function song21Play() {
  if(!song21.isPlaying()) {
    song21.play();
    button21.html("pause 21");
  } else {
    song21.stop();
    button21.html("play 21");
  }
}

function loaded() {
  console.log("loaded");
}
