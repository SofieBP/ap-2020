# AP 2020

**Screenshot of my MiniEx5**
![Screenshot](MiniEx5.PNG)

[Link](http://SofieBP.gitlab.io/ap-2020/MiniEx5)

**Link to the location of the index.html and j.s file**
https://gitlab.com/SofieBP/ap-2020/-/tree/master/public/MiniEx5

**Link to the location of the index.html and j.s file for MiniEx1**
https://gitlab.com/SofieBP/ap-2020/-/tree/master/public/MiniEx1



**What have you changed and why?**
For this week miniex I have chosen to revisit my very first miniex. In my first miniex i made a bunch of ellipses with a very thick outline, with movement. I had made a purple-ish background and the ellipses were yellow. The code was very simple, and looking back it didn't show a lot of technique or concept as the ones I have made later on. When I made that very first sketch the whole world of programming and using functions and syntaxes was a bit scary for me. That’s why I wanted to make something really simple, and so I did.I remember thinking the ellipses looked like records or a loudspeaker.

I have chosen to stay with the theme of the ellipses, which I will explain further on why. I have changed the format of the canvas to being windowWidth and Height, so I can have more ellipses. I have added buttons to line under all of the ellipses, as play and pause buttons, which plays different music depending on which button you press. I have used my own functions to make the music work when you press the button. I have used DOM-elements with the focus on buttons. I have also changed the color of the ellipses to random, so that it changes color in the entire spectrum og RGB.  


**Concept**
My concept for this MiniEx is to add on elements and layers to my former miniex. I have stayed with the record or loudspeaker looking program. I have made several more ellipses/records. The thing here is that I have added the element of sound, which appears once you interact with a button. The sounds are different from each other, and can only be played once you press them. I have changed the color so that it's random, to give it a more colorful and more festive look so that it feels more as if it's records. I decided on going with this miniex because I knew I could improve some aspects of the program. It was the first program so I had no clue on what I was doing, and had used very few syntaxes. This time I have been able to add the DOM-elements with the buttons and sound. I can clearly see how my coding experience has evolved from then to now. I am far more capable of using syntaxes and functions I would view as difficult or impossible back when we first began. Knowing what I know now about aesthetic programming it has become clear that evolution is a big part of it. Programming is a practice where you can constantly learn more and evolve to do even greater than you have before. And that doesn't stop. And with the evolution we also learn how to include digital culture in our programming so that it becomes more than just writing some code and voila here's a result. It's not just about creating something that's aesthetically pretty, but something that brings more meaning to the table, something that connects what happens around us.

As mentioned before, my first miniex didn't have a concept, it was more so about dipping my toes in the water of programming. My concept for this week is evolution and what I have been able to change about a program with a very limitless theme. I feel as if it has become better, and more up to date in terms of my knowledge. 
