function setup() {
  createCanvas(windowWidth, windowHeight);
   frameRate(8); // Sets the speed of the peacock feathers
}

function draw() {
 push();
 noStroke();

 // The body of the peacock
  fill(0, 0, 255); // Set fill to blue
    ellipse(width/1.99, height/2.4 ,30,30); // The head
     ellipse(width/2, height/2 ,50,55); // The body
      rect(width/2.009, height/2.4 ,12,40) // The neck

  // The white outer circle is the eye socket of the right eye
  ellipseMode(RADIUS); // Set ellipseMode to RADIUS
    fill(255); // Set fill to white
     ellipse(width/1.97, height/2.45, 5, 5);

 // The white outer circle is the eye socket of the left eye
ellipseMode(RADIUS); // Set ellipseMode to RADIUS
   fill(255); // Set fill to white
    ellipse(width/2.001, height/2.45, 5, 5);

// The pupil of the right eye
ellipseMode(CENTER); // Set ellipseMode to CENTER
  fill(0); // Set fill to black
   ellipse(width/1.966, height/2.45, 4.8, 4.8);

// The pupil of the left eye
ellipseMode(CENTER); // Set ellipseMode to CENTER
  fill(0); // Set fill to black
   ellipse(width/1.996, height/2.45, 4.8, 4.8);

// The white dot in the right pupil
fill(255); // Set fill to white
  ellipse(width/1.97, height/2.45 ,3 ,3);

// The white dot in the left pupil
fill(255); // Set fill to white
  ellipse(width/2, height/2.45 ,3.9 ,3.9);

// The beak
fill(255, 204, 0); // Set the color yellow
  ellipse(width/1.99, height/2.36, 4, 8);
   ellipse(width/1.99, height/2.36, 6, 5.45);

// The nosestrills
fill(0)
  circle(width/1.993, height/2.37, 0.9);
   circle(width/1.988, height/2.37, 0.9);
pop();

  fill(150,80); // Alpha value
  noStroke();
  rect(0, 0, width, height); // Background
  drawElements(); // Will create the throbber
}

function drawElements() {
  let num = 8
   push();
    translate(width/2, height/2); //this moves things to the center // 205/num >> degree of each ellipse' move ;frameCount%num >> get the remainder that indicates the movement of the ellipse
  let cir = -205/num*(frameCount%num); //this is to know which one among 8 possible positions // the minus infront of the degrees changes the throbber now that it isn´t 360 degrees
   rotate(radians(cir));
    noStroke();

     fill(0,0,0);
      ellipse(38, 0, 20, 1); //first line
       ellipse (42, 0, 10, 1); // second line
        ellipse (50, 0, 10, 1); //third line
         ellipse(80, 0, 10, 1); // Fourth line
          ellipse(120, 0, 20, 1); // Fifth line
           ellipse(190, 0, 50, 1); // Sixth line

    // The moving dots, x is the distance from the center
stroke(0, 0, 100); // Set strokecolor to dark blue
  strokeWeight(1);
    fill(0,100,0); // Set fill to green
      ellipse(65, 0, 22, 22); // First layer of feathers 
  noStroke();
    fill(255, 204, 0); // Set fill to yellow
      ellipse(65, 0, 13, 13);
    fill(0, 100, 255); // Set fill to light blue
      ellipse(65, 0, 8, 8);
    fill(0, 0, 100); // Set fill to dark blue
      ellipse(65, 0, 3.5, 3.5);

stroke(0, 0, 100); // Set strokecolor to dark blue
  strokeWeight(1);
    fill(0,100,0); // Set fill to green
      ellipse(100, 0, 32, 32); // Second layer of feathers
  noStroke();
    fill(255, 204, 0); // Set fill to yellow
      ellipse(100, 0, 22, 22);
    fill(0, 100, 255); // Set fill to light blue
      ellipse(100, 0, 14, 14);
    fill(0, 0, 100); // Set to dark blue
      ellipse(100, 0, 6.5, 6.5);

 stroke(0, 0, 100); // Set strokecolor to dark blue
   strokeWeight(1);
    fill(0,100,0); // Set fill to green
      ellipse(150,0,43,43); // Third layer of feathers
  noStroke();
    fill(255, 204, 0); // Sett fill to yellow
     ellipse(150, 0, 30, 30);
    fill(0, 100, 255); // Set fill to light blue
     ellipse(150, 0, 20, 20);
    fill(0, 0, 100); // Set fill for dark blue
     ellipse(150, 0, 10, 10);

 stroke(0, 0, 100);
   strokeWeight(1);
    fill(0, 100, 0); // Set fill to green
     ellipse(215, 0, 55, 55); // Fourth layer of feathers 
  noStroke();
    fill(255, 204, 0); // Set fill to yellow
     ellipse(215, 0, 40, 40);
    fill(0, 100, 255); // Set fill to light blue
     ellipse(215, 0, 28, 28);
    fill(0, 0, 100); // Set fill to dark blue
     ellipse(215, 0, 15, 15);
  pop();
}

function windowResized () {
 resizeCanvas(windowWidth, windowHeight);
}
