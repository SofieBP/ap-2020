# AP 2020
![Screenshot](MiniEx3.PNG)

[Link](http://SofieBP.gitlab.io/ap-2020/MiniEx3)

**Link to the location of the index.html and j.s file**
https://gitlab.com/SofieBP/ap-2020/-/tree/master/public/MiniEx3

**DISCLAIMER** 
The body of the peacock might look weird when the link is opened. On my own computer it looks the way it should, but I tried opening it on another where it looked strange. I have had some difficulties placing the body, even though I have used width and height to center it like the throbber. 

**What are the time-related syntaxes/functions that you use in this program? Why do you use them in this way?**

For this mini exercise I have used Winnies throbber as an inspiration in terms of syntaxes for the movement of the throbber. It made a lot of sense for me, also since I have had some difficulties with understanding and explaning syntaxes prior. I have then found out to create a different type of throbber. I played around with shapes and colors and accidently made it look like peacock feathers which made me stick to the idea of creating a peacock throbber.

I have used the syntax for ellipse a lot. I used it to create the body of the peacock and the feathers. I used the syntax for rect as well, fill, stroke, noStroke strokeWeight, push, pop, frameRate, ellipseMode, circle, function drawElements, let num, let cir, function windowResized. Originally I wanted to use beginShape and endShape in order to create the bird but somehow I couldn´t get it to work and when I made lines they disapeared due to the constant overlap/draw over of background and throbber. I insteaad went with the ellipses to create, it was easier to manage and easier to combine with width/x, height/x than other shapes. I will probably work more on that to make it work. 

When making the feathers which worked as the throbber I didn´t want it to rotate 360 degrees, it should look like when a peacock spread its feathers. I then made it rotate half way, which was only 180 degrees. However the problem here was that the throbber rotated in the wrong direction; it went down instead of up. I put a minus in front of the degreee number and made it the way I wanted, and then I played around with the degrees in order to get it as close to a perfect half circle. I ended on 205 degrees which I was happy with in the end. The peacock could probably look more like a peacock (it´s all about the details) but I stil think that the idea shines through pretty well. I wanted to make sure I was using syntaxes and functions I understood, which is why it isn´t filled with plenty of complex syntaxes or functions. 


**What does a throbber tells us,and/or hides? How might we think about this remarkable throbber icon differently?** 
A throbber indicates that a page or a video is loading and that we should wait for a moment. We see it many places such as on Facebook, Youtube, Netflix and so on. It indicates that the data isn´t quite ready yet but will be. When it seems like it takes forever to load we become impatient and sometimes end up clicking all over the screen which can confuse the machine even more. I find that it´s easier to become impatient and irritated with a throbber when it´s a plain and boring one. If it´s just circles or a line rotating around it can feel like it takes forever. If the throbber either looks more interesting or is interactive perhaps people would be more patient? I wanted my throbber to look more fun and interesting, that´s why I made the peacock as the throbber. We might think about the icon differently if it gives us something different than what we are used to. 