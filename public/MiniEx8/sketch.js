let star = []; // array for the stars/ellipses that
let min_star = 50; // minumum amount of stars on canvas
var thoughts; // variable for thoughts - refers to json file
let livingBeing //  alien
let audio // audio that plays in the background

function preload(){
  thoughts = loadJSON('thought.json');
  livingBeing = loadImage('alien.png');
  audio = loadSound("audiothoughts.mp4");
}

function setup() {
    audio.loop();
    createCanvas(1500, 700);
    frameRate(60);

  for (let i = 0; i <= min_star; i++){
    star[i] = new Star();
  }
}

function draw() {
  background(20, 10, 0);

// the circles going towards the center of the canvas
  push();
    fill(255,200,0,30);
    noStroke();
    circle(width/2,height/2,1000);
    circle(width/2,height/2,800);
    circle(width/2,height/2,600);
    circle(width/2,height/2,400);
  pop();

    console.log(mouseX, mouseY);

// alien figure in the middle of the canvas
    image(livingBeing, 500, 80, 500, 500);

// the text displayed on canvas
    push()
      translate(width/2, height/2);
      rectMode(CENTER);
      strokeWeight(0.5);
      fill(255, 200,0, 0);
      textSize(20);
      rotate(radians(45));
      text(thoughts.t1[0], -500,0);
      rotate(radians(5));
      text(thoughts.t2[0], -500,0);
      rotate(radians(5));
      text(thoughts.t3[0], -500,0);
      rotate(radians(5));
      text(thoughts.t1[0], -500,0);
      rotate(radians(5));
      text(thoughts.t2[0], -500,0);
      rotate(radians(5));
      text(thoughts.t3[0], -500,0);
      rotate(radians(5));
      text(thoughts.t1[0], -500,0);
      rotate(radians(5));
      text(thoughts.t2[0], -500,0);
      rotate(radians(5));
      text(thoughts.t3[0], -500,0);
      rotate(radians(5));
      text(thoughts.t1[0], -500,0);
      rotate(radians(5));
      text(thoughts.t2[0], -500,0);
      rotate(radians(5));
      text(thoughts.t3[0], -500,0);
      rotate(radians(5));
      text(thoughts.t1[0], -500,0);
      rotate(radians(5));
      text(thoughts.t2[0], -500,0);
      rotate(radians(5));
      text(thoughts.t3[0], -500,0);
      rotate(radians(5));
      text(thoughts.t1[0], -500,0);
      rotate(radians(5));
      text(thoughts.t2[0], -500,0);
      rotate(radians(5));
      text(thoughts.t3[0], -500,0);
      rotate(radians(5));
      text(thoughts.t1[0], -500,0);
    pop()

// calls the showStar functions for the moving ellipses on canvas
    showStar();
  for (let i = 0; i<star.length;i++){
    star[i]=new Star();
    star[i].show();
  }
}

function showStar(){
  for (let i = 0; i <star.length;i++) {
    star[i].show();
    star[i].move();
  }
}

class Star {
  constructor(x, y, size){
    this.sizeX = random (1,10);
    this.sizeY = random(10,50);
    this.startSize = this.size
    this.Xpos = random(width);
    this.Ypos = random(height);
  }

  move() {
    this.x, this.y
  }

  show() {
    stroke(255, 200, 0);
    strokeWeight(1);
    noFill();
    ellipse(this.Xpos, this.Ypos, this.sizeX, this.sizeY);
  }
}
