# AP 2020

![Screenshot](MiniEx6.PNG)

[Link](http://SofieBP.gitlab.io/ap-2020/MiniEx6)

**Link to the location of the index.html and j.s file**
https://gitlab.com/SofieBP/ap-2020/-/tree/master/public/MiniEx6

**Inspiration**
https://editor.p5js.org/ehersh/sketches/Hk52gNXR7



**Describe how does your game/game objects work?**

My program for this MiniEx is a game where the aim is to catch the falling sushi pieces. The character the player is playing is a hungry cat. I have used mouseX as to control the cat so that when you drag the mouse across the screen, the cat moves. Then I also made sure that when the sushi pieces hit a certain point of the cat (its bowl) the points will be counted. If the sushi doesn’t hit the cat you lose the game and can try again. I have added background music to make the game more fun and a sound effect for when the cat eats the sushi pieces and when it’s game over. 

I found inspiration for the game online where I came across a catching game. In that game you had to catch a falling ball. For the entire process I wanted to make a catching game but found it difficult figuring out how to. The game I found gave me great inspiration in terms of creating different states (screens) for the game; a start, the game itself and an end. I also looked through Winnies code and some of the other students from the class in order to understand how to use class in games and make it run like a game would. I I have made it my own program, but it helped me understand it better by looking at how others have made theirs. 


**Describe how you program the objects and their related attributes and methods in your game**

I have used **class** to create my sushi. The sushi in the game is one I have created myself with rectangles creating the fish, big rice piece and nori (seaweed). The ellipses created the small rice detail. The reason why I mention this  is that I had issues with the shapes in the class and making them appear in the game the same way, so I took a screenshot of the sushi piece and saved as an image which I have put in instead. 

I wanted it to look simple yet easily recognizable. I pictured a piece of sushi with seaweed, rice and a piece of fish on. I then began by designing the piece of sushi how I wanted it to look. Afterwards I began on my class which contains the speed of the sushi with a speed between 2 and 10, I didn’t want it to go too slow or too fast. I also used the position of the sushi, size and movement in my class.    

The cat is an animated gif I found online. I wanted to make a cat myself as well, but I had several issues with running the program, so I decided to find an image which was still simple as what I had in mind. Same way with the sushi, I wanted it to look simple yet recognizable. I went with the cartoon look for both, giving it a computer game look. Here I didn’t use a class, in the same way as Winnie’s pacman. I wanted it to move along the x-axe when the mouse is being dragged.


**What are the characteristics of object-oriented programming and the wider implications of abstraction?**

This week we read “How To Be a Geek – Essay on the Culture of Software” by Matthew Fuller. This essay related on Object-oriented programming, which was this week’s focus, and have been a focus on the MiniEx as well. 

With Object-oriented programming and important part is abstraction. This is how you create the different layers of an object. Object abstraction in computing is about representation. With an object there’s different attributes that decides what the object is, these attributes help defining the object. Certain attributes and relations are abstracted from the real world, and at the same time, some details are left out. In our Tuesday lesson we used a person as an example. For a person some attributes we could list would be ex. hair color, height, favorite food, name, age. There’s different characteristics and behaviors that a person embodies. In coding we use the name class to give an overview of the object’s properties and behaviors. We list all those things in the class and can later be used. It’s a so-called pseudo class we create after listing up all the things about the object. It’s possible to make several object instance with the same properties from a class, we can reuse.


**Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?**

As programmers we choose the attributes that are absolutely necessary in order to get the program to work in the way we wish for them to work. There’s no such thing as a universal picture of what that looks like. 

We make choices on the aspects we want to be represented in our objects and which we leave out. We thereby create a picture of what a certain object looks like and how it should be understand to other people. We have an understanding of what certain objects are and look like before we begin programming, and that will play a part in how we create. 

For me I have chosen that the game appears simple to the user with few instructions even though it was difficult to create. I also chose the computer game look with brigth colored backgrounds and change of the background when something happens as if it was another game. I also added specific sound effects such as a chew sound, cat sound and background music to make it more game like. And by my objects I have given a picture of what a cat and a piece of a sushi can look like, what they look like in my game.