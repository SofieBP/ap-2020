class Sushi {
  constructor (x, y, size) {
      this.speed = -floor(random(2,10));
      this.x = x;
      this.y = y;
      this.size = size;
  }
  move() {
    this.y-=this.speed;
  }
  show() { //Show sushi
    image(imgsushi, this.x, this.y, this.size, this.size);
  }
}
