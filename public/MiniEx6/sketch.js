let i = 0;
let sushi = [];
let min_sushi = 1;
let imgsushi;
let imgcat;
let points= 0;
let state = 0;
var song;
var noise;
var chew;

function preload () {
  imgsushi = loadImage('sushi.PNG')
  imgcat = loadImage("cat.gif")
  song = loadSound("backgroundmusic.mp3")
  noise = loadSound("catnoise.mp3")
  chew = loadSound("chew.mp3")
}

function setup() {
  song.loop();
  createCanvas(1000, 600);
  for(let i=0; i<=min_sushi; i++){
    sushi[i] = new Sushi(random(100,1000),-50,50); // (x-cordinate,y-cordinate where sushi spawns,size)
  }
}

function draw() {
  background(255);
  if(state == 0) { // if statement for the state switch. 0 = stateState, 1 = gameState, 2 = endState
    startState()
  } else if (state == 1) {
    gameState()
  } else if(state == 2) {
    endState()
  }
}

function startState () { // This is the first state the player meets
  background(255, 255, 102);
  textAlign(CENTER);
  fill(0);
  textSize(50);
  text('WELCOME TO SUSHI CATCH', width / 2, height / 2);
  textSize(30);
  text('The rules of the game are simple', width / 2, height / 2 + 60);
  textSize(28);
  text('The cat is very hungry and would like some sushi', width / 2, height / 2 + 120);
  text('Use the mouse to move the cat and catch the sushi falling down the screen', width / 2, height / 2 + 150);
  text('CLICK TO BEGIN', width / 2, height / 2 + 220);
  reset();
}

function gameState () { // The state where the game is
  background(102, 255, 102);
  fill(0);
  textSize(10);
  text('Points = ' + point, 30, 20);
  image(imgcat,mouseX,height-90,100,100) // (cat placement, width, height)
  for (let i=0; i<sushi.length; i++){
    sushi[i].move();
    sushi[i].show();
  }
  if(sushi[i].y>height){
    sushi.splice(i,1);
    newsushi();
    state = 2 // switch to endState = game over
    noise.play();
  }
  if(sushi[i].y>height-100 && sushi[i].x>mouseX-50 && sushi[i].x<mouseX+70){ // (if it's below the cat placement, last two numbers are from where sushi hits on cat)
    point++;
    sushi.splice(i,1);
    newsushi();
    chew.play();
  }
}

function endState() { // The end state the player meets before pressing the screen to play again
  background(255, 102, 102);
  textAlign(CENTER);
  fill(0);
  textSize(50);
  text('GAME OVER', width / 2, height / 2)
  textSize(30);
  text("Total points = " + point, width / 2, height / 2 + 60)
  text('click anywhere to play again', width / 2, height / 2 + 100);
}

function mousePressed() { // mousePressed to change the different game states
  if(state == 0) {
    state=1
  } else if(state == 2) {
    state = 0
  }
}

function reset(){
  point=0;
}

function newsushi() {
  if (sushi.length < min_sushi) {
    sushi.push(new Sushi(random(100,1000),-50,50));
  }
}
